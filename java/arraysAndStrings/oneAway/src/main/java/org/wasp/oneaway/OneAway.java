package org.wasp.oneaway;

class OneAway {
    
    static boolean validate(String str1, String str2) {
        if(Math.abs(str1.length() - str2.length()) > 1) return false;
        
        String shortString = str1.length() > str2.length() ? str2 : str1;
        String longString = str1.length() > str2.length() ? str1 : str2;
        
        int shortIndex = 0;
        int longIndex = 0;
        boolean moreThanOne = false;
        while(shortIndex < shortString.length() && longIndex < longString.length()) {
            if(shortString.charAt(shortIndex) != longString.charAt(longIndex)) {
                if(moreThanOne) return false;
                moreThanOne = true;
                if(shortString.length() == longString.length()) shortIndex++;
            } else {
                shortIndex++;
            }
            longIndex++;
        }
        return true;
    }
}
