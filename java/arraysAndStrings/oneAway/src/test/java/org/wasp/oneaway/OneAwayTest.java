package org.wasp.oneaway;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class OneAwayTest {
    
    private String str1;
    private String str2;
    private boolean expected;

    public OneAwayTest(String str1, String str2, boolean expected) {
        this.str1 = str1;
        this.str2 = str2;
        this.expected = expected;
    }
    
    @Parameterized.Parameters(name = "{index} OneAway({0}, {1}) == {2}")
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][] {
                {"pale", "pales", true},
                {"pales", "pale", true},
                {"pale", "bale", true},
                {"pale", "bake", false},
                {"pale", "bales", false},
                {"pale", "ple", true},
                {"pale", "ble", false}
        });
    }

    @Test
    public void oneAwayTest() {
        Assert.assertThat(OneAway.validate(str1, str2), CoreMatchers.is(expected));
    }
    
}
