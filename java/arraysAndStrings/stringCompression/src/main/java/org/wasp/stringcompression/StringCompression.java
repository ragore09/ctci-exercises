package org.wasp.stringcompression;

class StringCompression {
    
    static String compress(String input) {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for(int i = 0; i < input.length(); i++) {
            counter++;
            if(i + 1 == input.length() || input.charAt(i+1) != input.charAt(i)) {
                stringBuilder.append(input.charAt(i));
                stringBuilder.append(counter);
                counter = 0;
            }
        }
        return stringBuilder.length() > input.length() ? input : stringBuilder.toString();
    }
    
}
