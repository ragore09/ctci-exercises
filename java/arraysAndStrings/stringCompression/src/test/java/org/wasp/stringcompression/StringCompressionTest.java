package org.wasp.stringcompression;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class StringCompressionTest {
    
    private String input;
    private String expected;

    public StringCompressionTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }
    
    @Parameterized.Parameters(name = "{index} StringCompression({1} = {2})")
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][] {
                {"aabccccczzz", "a2b1c5z3"},
                {"aaaarrrtta", "a4r3t2a1"},
                {"hola", "hola"},
                {"queso", "queso"},
        });
    }
    
    @Test
    public void stringCompressionTest() {
        Assert.assertThat(StringCompression.compress(input), CoreMatchers.is(expected));
    }
}
