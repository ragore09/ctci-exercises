package org.wasp.checkPermutation;

class IsPermutation {
    
    static boolean validate(String str1, String str2) {
        if(str1.length() != str2.length()) return false;
        
        int charSet[] = new int[128];
        for(int i = 0; i < str1.length(); i++) {
            charSet[str1.charAt(i)]++;
        }

        for(int i = 0; i < str2.length(); i++) {
            charSet[str2.charAt(i)]--;
            if(charSet[str2.charAt(i)] < 0) return false;
        }
        return true;
    } 
}
