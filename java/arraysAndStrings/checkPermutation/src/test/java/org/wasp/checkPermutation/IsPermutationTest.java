package org.wasp.checkPermutation;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class IsPermutationTest {
    
    private String str1;
    private String str2;
    private boolean expected;
    
    public IsPermutationTest(String str1, String str2, boolean expected) {
        this.str1 = str1;
        this.str2 = str2;
        this.expected = expected;
    }
    
    @Parameters(name = "{index}: isPermutation({0}, {1}) = {2}")
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][] {
                {"azb", "baz", true}, 
                {"abbccc", "cbbacc", true}, 
                {"acbcbc", "cbcbdc", false},
                {"aloha", "hola", false},
                {"que es eso?", "eso es ?que", true}
        });
    }
    
    @Test
    public void checkPermutations() {
        Assert.assertThat(IsPermutation.validate(str1, str2), CoreMatchers.is(expected));
    }
    
}
