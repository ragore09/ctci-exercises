package org.wasp.palindromepermutation;

class PalindromPermutation {
    
    static boolean validate(String input) {
        int checker = 0;
        for(char c : input.toCharArray()) {
            if(c >= 'a' && c <= 'z') checker ^= 1<<(c-'a');
        }
        return (checker & (checker - 1)) == 0;
    }
}
