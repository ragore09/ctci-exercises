package org.wasp.palindromepermutation;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class PalindromePermutationTest {
    
    private String input;
    private boolean expected;

    public PalindromePermutationTest(String input, boolean expected) {
        this.input = input;
        this.expected = expected;
    }
    
    @Parameterized.Parameters(name = "{index} PalindromPermutation({0}) = {1}")
    public static Collection<Object[]> testCases() { 
        return Arrays.asList(new Object[][] {
                {"acto cat", true},
                {"ramoa mora", true},
                {"ahgyd duii", false},
                {"tania alva a atinl", true}
        });
    }

    @Test
    public void palindromePermutationTest() { 
        Assert.assertThat(PalindromPermutation.validate(input), CoreMatchers.is(expected));
    }
    
}
