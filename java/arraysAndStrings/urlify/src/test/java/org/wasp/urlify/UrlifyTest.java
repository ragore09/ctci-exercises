package org.wasp.urlify;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class UrlifyTest {
    
    private char[] input;
    private int length;
    private char[] expected;
    
    public UrlifyTest(char[] input, int length, char[] expected) {
        this.input = input;
        this.length = length;
        this.expected = expected;
    }
    
    @Parameterized.Parameters(name = "{index}: urlify({0}, {1}) = {2}")
    public static Collection<Object[]> testCases(){
        return Arrays.asList(new Object[][] {
                {"My name is Raul         ".toCharArray(), 15, "My%20name%20is%20Raul".toCharArray()}, 
                {"hello how are you?        ".toCharArray(), 18, "hello%20how%20are%20you?".toCharArray()},
                {"I want the new iPhone X           ".toCharArray(), 23, "I%20want%20the%20new%20iPhone%20X".toCharArray()},
                {"I'm hungry       ".toCharArray(), 10, "I'm%20hungry".toCharArray()}
        });
    }

    @Test
    public void urlifyTest() {
        Assert.assertThat(String.valueOf(Urlify.urlify(input, length)).trim(), CoreMatchers.is(String.valueOf(expected)));
    }
    
    
}
