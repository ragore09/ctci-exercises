package org.wasp.urlify;

class Urlify {
    
    static char[] urlify(char[] input, int length) {
        int spaceCount = 0;
        for(int i = 0; i < length; i++) {
            if(input[i] == 32) spaceCount++;
        }
        
        int index = length + 2 * spaceCount - 1;
        for(int i = length - 1; i >= 0; i--) {
            if(input[i] != 32) {
                input[index] = input[i];
                index--;
            } else {
                input[index] = '0';
                input[index-1] = '2';
                input[index-2] = '%';
                index-=3;
            }
        }
        return input;
    }
}
