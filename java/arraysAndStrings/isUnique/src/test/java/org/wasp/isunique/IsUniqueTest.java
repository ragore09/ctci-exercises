package org.wasp.isunique;

import org.junit.Assert;
import org.junit.Test;

public class IsUniqueTest {
    
    private static final String UNIQUE_CHARACTERS_1 = "qwertyuiopasdfghjklzxcvbnm";
    private static final String UNIQUE_CHARACTERS_2 = "mkonjibhuvgycftxdrzse";
    private static final String UNIQUE_CHARACTERS_3 = "mkoxsqnjicdw";
    private static final String NOT_UNIQUE_CHARACTERS_1 = "qwertyytrewq";
    private static final String NOT_UNIQUE_CHARACTERS_2 = "mkonjibhuvgycftazaq";
    
    @Test
    public void allUniqueCharactersTest() {
        Assert.assertTrue(IsUnique.validate(UNIQUE_CHARACTERS_1));
        Assert.assertTrue(IsUnique.validate(UNIQUE_CHARACTERS_2));
        Assert.assertTrue(IsUnique.validate(UNIQUE_CHARACTERS_3)); 
        Assert.assertTrue(IsUnique.validateBytes(UNIQUE_CHARACTERS_1));
        Assert.assertTrue(IsUnique.validateBytes(UNIQUE_CHARACTERS_2));
        Assert.assertTrue(IsUnique.validateBytes(UNIQUE_CHARACTERS_3));
    }
    
    @Test
    public void notUniqueCharactersTest() {
        Assert.assertFalse(IsUnique.validate(NOT_UNIQUE_CHARACTERS_1));
        Assert.assertFalse(IsUnique.validate(NOT_UNIQUE_CHARACTERS_2)); 
        Assert.assertFalse(IsUnique.validateBytes(NOT_UNIQUE_CHARACTERS_1));
        Assert.assertFalse(IsUnique.validateBytes(NOT_UNIQUE_CHARACTERS_2));
    }
    
}
