package org.wasp.isunique;

/**
 * Algorithm to determine if a string has all unique characters
 */
class IsUnique {

    /**
     * Validates using a boolean array
     */
    static boolean validate(String input) {
        if(input.length() > 26) return false;
        boolean[] char_set = new boolean[32];
        for(int i = 0; i < input.length(); i++) {
            int ascii = input.charAt(i) - 'a';
            if(char_set[ascii]) return false;
            char_set[ascii] = true;
        }
        return true;
    }

    /**
     * Validates using bit operations
     */
    static boolean validateBytes(String input) {
        if(input.length() > 26) return false;
        int checker = 0;
        for(int i = 0; i < input.length(); i++) {
            int ascii = input.charAt(i) - 'a';
            if((checker & (1 << ascii)) > 0) return false;
            checker |= 1 << ascii;
        }
        return true;
    }

}
