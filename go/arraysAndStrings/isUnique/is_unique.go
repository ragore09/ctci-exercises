package isUnique

func Validate(input string) bool {
	if len(input) > 26 {
		return false
	}
	var char_set [32]bool
	for _, ascii := range input {
		ascii -= 'a'
		if char_set[ascii] {
			return false
		}
		char_set[ascii] = true
	}
	return true
}

func ValidateBytes(input string) bool {
	if len(input) > 26 {
		return false
	}
	var checker rune
	for _, ascii := range input {
		ascii -= 'a'
		if checker&(1<<uint32(ascii)) > 0 {
			return false
		}
		checker |= 1 << uint32(ascii)
	}
	return true
}
