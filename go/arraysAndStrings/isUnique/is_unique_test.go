package isUnique

import "testing"

type testCase struct {
	input    string
	expected bool
}

var testCases = []testCase{
	{"qwertyuiopasdfghjklzxcvbnm", true},
	{"mkonjibhuvgycftxdrzse", true},
	{"mkoxsqnjicdw", true},
	{"qwertyytrewq", false},
	{"mkonjibhuvgycftazaq", false},
}

func TestUniqueCharacters(t *testing.T) {
	for _, test := range testCases {
		actual := Validate(test.input)
		if actual != test.expected {
			t.Errorf("Validation failed for [%s] - expected [%t], got [%t]", test.input, test.expected, actual)
		}
	}
}

func TestUniqueCharactersUsingBit(t *testing.T) {
	for _, test := range testCases {
		actual := ValidateBytes(test.input)
		if actual != test.expected {
			t.Errorf("Validation failed for [%s] - expected [%t], got [%t]", test.input, test.expected, actual)
		}
	}
}
