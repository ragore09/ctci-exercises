package palindromePermutation

import (
	"testing"
)

type testCase struct {
	input    string
	expected bool
}

var testCases = []testCase{
	{"acto cat", true},
	{"ramoa mora", true},
	{"ahgyd duii", false},
	{"tania alva a atinl", true},
}

func TestIsPalindromePermutation(t *testing.T) {
	for _, testCase := range testCases {
		actual := PalindromePermutation(testCase.input)
		if actual != testCase.expected {
			t.Errorf("error for [%s], got [%t] - expected [%t]", testCase.input, actual, testCase.expected)
		}
	}
}
