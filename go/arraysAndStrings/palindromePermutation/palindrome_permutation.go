package palindromePermutation

func PalindromePermutation(input string) bool {
	var checker rune
	for _, ascii := range input {
		if ascii >= 'a' && ascii <= 'z' {
			checker ^= 1 << (uint(ascii) - 'a')
		}
	}
	return checker&(checker-1) == 0
}
