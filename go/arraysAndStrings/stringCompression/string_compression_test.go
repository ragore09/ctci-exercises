package stringCompression

import "testing"

type testCase struct {
	input string
	expected string
}

var tests = []testCase{
	{"aabccccczzz", "a2b1c5z3"},
	{"aaaarrrtta", "a4r3t2a1"},
	{"hola", "hola"},
	{"queso", "queso"},
}

func TestStringCompression(t *testing.T) {
	for _, test := range tests {
		actual := StringCompression(test.input)
		if actual != test.expected {
			t.Errorf("for [%s] was expected [%s] - got [%s]", test.input, test.expected, actual)
		}
	}
}

func BenchmarkStringCompression(b *testing.B) {
	for _, test := range tests {
		for i := 0; i < b.N; i++ {
			StringCompression(test.input)
		}
	}
}