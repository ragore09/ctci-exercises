package stringCompression

import (
	"bytes"
	"strconv"
)

func StringCompression(input string) (final string) {
	var buffer bytes.Buffer
	var temporal rune
	var counter int

	for _, ascii := range input {
		if ascii != temporal {
			if counter != 0 {
				buffer.WriteString(strconv.Itoa(counter))
			}
			counter = 0
			buffer.WriteRune(ascii)
			temporal = ascii
		}
		counter++
	}
	buffer.WriteString(strconv.Itoa(counter))


	if final = buffer.String(); len(final) > len(input) {
		final = input
	}
	return
}
