package oneAway

import "testing"

type testCase struct {
	str1     string
	str2     string
	expected bool
}

var tests = []testCase{
	{"pale", "pales", true},
	{"pales", "pale", true},
	{"pale", "bale", true},
	{"pale", "bake", false},
	{"pale", "bales", false},
	{"pale", "ple", true},
	{"pale", "ble", false},
}

func TestOneAway(t *testing.T) {
	for _, test := range tests {
		actual := OneAway(test.str1, test.str2)
		if actual != test.expected {
			t.Errorf("error for OneAway([%s], [%s]) - expected [%t] got [%t]",
				test.str1, test.str2, test.expected, actual)
		}
	}
}

func BenchmarkOneAway(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			OneAway(test.str1, test.str2)
		}
	}
}
