package oneAway

import "math"

func OneAway(str1, str2 string) bool {
	if math.Abs(float64(len(str1)-len(str2))) > 1 {
		return false
	}
	var shortString string
	var longString string
	if shortString = str1; len(str1) > len(str2) {
		shortString = str2
	}
	if longString = str2; len(str1) > len(str2) {
		longString = str1
	}

	shortIndex, longIndex := 0, 0
	moreThanOne := false
	for shortIndex < len(shortString) && longIndex < len(longString) {
		if shortString[shortIndex] != longString[longIndex] {
			if moreThanOne {
				return false
			}
			moreThanOne = true
			if len(shortString) == len(longString) {
				shortIndex++
			}
		} else {
			shortIndex++
		}
		longIndex++
	}
	return true
}
