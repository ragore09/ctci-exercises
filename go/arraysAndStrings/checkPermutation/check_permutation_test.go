package checkPermutation

import (
	"testing"
)

type testCase struct {
	str1     string
	str2     string
	expected bool
}

var testCases = []testCase{
	{"azb", "baz", true},
	{"abbccc", "cbbacc", true},
	{"acbcbc", "cbcbdc", false},
	{"aloha", "hola", false},
	{"que es eso?", "eso es ?que", true},
}

func TestCheckPermutation(t *testing.T) {
	for _, testCase := range testCases {
		actual := CheckPermutation(testCase.str1, testCase.str2)
		if actual != testCase.expected {
			t.Errorf("Validation failed for [%s] [%s]- expected [%t], got [%t]", testCase.str1, testCase.str2,
				testCase.expected, actual)
		}
	}
}
