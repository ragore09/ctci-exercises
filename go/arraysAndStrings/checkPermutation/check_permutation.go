package checkPermutation

func CheckPermutation(str1, str2 string) bool {
	if len(str1) != len(str2) {
		return false
	}
	var charSet [128]int
	for _, ascii := range str1 {
		charSet[ascii]++
	}

	for _, ascii := range str2 {
		charSet[ascii]--
		if charSet[ascii] < 0 {
			return false
		}
	}

	return true
}
