package urlify

func Urlify(input []rune, length int) []rune {
	var spaceCount int
	for i:=0;i < length; i++ {
		if input[i] == 32 {
			spaceCount++
		}
	}

	index := length + 2 * spaceCount - 1
	for i:=length-1; i>=0; i-- {
		if input[i] != 32 {
			input[index] = input[i]
			index--
		} else {
			input[index] = '0'
			input[index-1] = '2'
			input[index-2] = '%'
			index-=3
		}
	}
	return input
}
