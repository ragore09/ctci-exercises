package urlify

import (
	"testing"
	"fmt"
	"strings"
)

type testCase struct {
	input []rune
	length int
	expected []rune
}

var testCases = []testCase {
	{[]rune("My name is Raul         "), 15, []rune("My%20name%20is%20Raul")},
	{[]rune("hello how are you?        "), 18, []rune("hello%20how%20are%20you?")},
	{[]rune("I want the new iPhone X                 "), 23, []rune("I%20want%20the%20new%20iPhone%20X")},
	{[]rune("I'm hungry       "), 10, []rune("I'm%20hungry")},
}

func TestUrlify(t *testing.T) {
	for _, testCase := range testCases {
		actual := Urlify(testCase.input, testCase.length)
		if strings.TrimSpace(string(actual)) != strings.TrimSpace(string(testCase.expected)) {
			t.Errorf("expected [%s], got [%s]", string(testCase.expected), string(actual))
		}
	}
}